FROM docker.io/python:3-slim-buster

# RadosGateway Exporter image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive

LABEL io.k8s.description="RadosGateway Prometheus Exporter Image." \
      io.k8s.display-name="RadosGateway Prometheus Exporter" \
      io.openshift.expose-services="9113:http" \
      io.openshift.tags="prometheus,exporter,ceph,radosgw" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-rgwexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.1.0"

RUN mkdir -p /usr/src/app /config
WORKDIR /usr/src/app
COPY config/* /usr/src/app/

RUN set -x \
    && mv run-exporter.sh / \
    && apt-get update \
    && apt-get install -y dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && pip install --no-cache-dir -r requirements.txt \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENTRYPOINT [ "dumb-init", "--", "/run-exporter.sh" ]
USER 1001
