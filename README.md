# k8s Prometheus RadosGateway Exporter

Forked from https://github.com/blemmenes/radosgw_usage_exporter

Build with:
```
$ make build
```

Build in OpenShift:

```
$ make ocbuild
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

## Ceph Prerequisites

Create Ceph RadosGW keyring & Prometheus s3 user:

```
# ceph auth get-or-create client.radosgw.gateway osd 'allow rwx' mon 'allow rwx' -o /etc/ceph/ceph.client.radosgw.gateway.keyring
# awk '/key =/{print $NF}' /etc/ceph/ceph/client.radosgw.gateway.keyring
# radosgw-admin user create --display-name=Prometheus --uid=prometheus
# radosgw-admin caps add --uid=prometheus "--caps=usage=read;buckets=read"
...
# oc process -f deploy/openshift/secret.yaml \
    -p CEPH_INITIAL_MEMBERS=mon1,mon2,mon3 \
    -p CEPH_CLUSTER_ID=f980b615-746a-4e5e-b429-a364fd69838b \
    -p CEPH_MON_HOSTS="[v2:10.42.253.110:3300,v1:10.42.253.110:6789],[v2:10.42.253.111:3300,v1:10.42.253.111:6789],[v2:10.42.253.112:3300,v1:10.42.253.112:6789]" \
    -p S3_USAGE_ACCESS_KEY=<prometheus-access-key> \
    -p S3_USAGE_SECRET_KEY=<prometheus-secret-key> \
    -p RADOSGW_KEYRING_KEY=<ceph-radosgw-key> \
    -p RADOSGW_KEYRING_NAME=radosgw.gateway | oc apply -f-
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name           |    Description            | Default       |
| :------------------------- | ------------------------- | ------------- |
|  `CLUSTER_NAME`            | Ceph Cluster Name         | `ceph`        |
|  `EXPORTER_PORT`           | RadosGW Exporter Port     | `9113`        |
|  `RADOSGW_HOST`            | RadosGW Endpoint Host     | `127.0.0.1`   |
|  `RADOSGW_PORT`            | RadosGW Endpoint Port     | `8080`        |
|  `RADOSGW_PROTO`           | RadosGW Endpoint Proto    | `http`        |
